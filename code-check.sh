#!/usr/bin/env bash

if [ ! -f "/usr/local/bin/codeclimate" ]
then
	curl -L https://github.com/codeclimate/codeclimate/archive/master.tar.gz | tar xvz -C /home/$USER/
	(cd /home/$USER/codeclimate-* && make install)
fi

codeclimate engines:install  ## install and setup from .codeclimate.yml
codeclimate analyze --format json > gl-code-quality-report.json

